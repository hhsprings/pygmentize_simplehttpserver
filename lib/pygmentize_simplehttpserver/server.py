# -*- coding: utf-8 -*-
#
# Copyright (c) 2015, hhsprings <https://bitbucket.org/hhsprings>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 
# - Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# 
# - Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution.
# 
# - Neither the name of the hhsprings nor the names of its contributors
#   may be used to endorse or promote products derived from this software
#   without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
import os
import sys
import pygments
import pygments.lexers
from pygments.lexers import guess_lexer_for_filename, guess_lexer
from pygments.lexers import get_lexer_by_name
from pygments.formatters.html import HtmlFormatter
from pygments.util import ClassNotFound


encodings = ["utf-8"]
if sys.stdout.encoding != "utf-8":
    encodings.append(sys.stdout.encoding)
if sys.stdout.encoding != "iso-8859-1":
    encodings.append("iso-8859-1")
encodings.append("ascii")


try:
    import StringIO
    StringIO = StringIO.StringIO
    BytesIO = StringIO
    import SimpleHTTPServer as simplehttpserver_mod
    import BaseHTTPServer as basehttpserver_mod
    import urllib
    def unquote(s):
        return urllib.unquote(s)
    def quote(s):
        return urllib.quote(s)
    import cgi
    escape = cgi.escape
    from urlparse import urlparse
    _PY27 = True
except ImportError:
    # python 3
    import io
    from io import StringIO
    from io import BytesIO
    import http.server as simplehttpserver_mod
    basehttpserver_mod = simplehttpserver_mod
    import urllib.parse  # Python 3.x
    def unquote(s):
        return urllib.parse.unquote(s)
    def quote(s):
        return urllib.parse.quote(s)
    import html
    escape = html.escape
    from urllib.parse import urlparse
    _PY27 = False


class Formatter(object):
    def __init__(self, **kwarg):
        self.options = dict(
            style=kwarg.get("style", "default"),
            linenos=kwarg.get("linenos", None),
            nobackground=kwarg.get("nobackground", "False") in ("True", "true", "1"),
            )

    def __call__(self, path, **kwarg):
        with open(path, 'rb') as f:
            code = f.read()
            for enc in encodings:
                try:
                    code = code.decode(enc)
                    break
                except UnicodeError:
                    pass
            formatter_params = dict(full=True)
            formatter_params.update(self.options)
            formatter_params.update(kwarg)
            if "lexer" in formatter_params:
                lx = formatter_params["lexer"]
                del formatter_params["lexer"]
                lexer = get_lexer_by_name(lx)
            else:
                try:
                    lexer = guess_lexer_for_filename(path, code)
                except ClassNotFound:
                    try:
                        lexer = guess_lexer(code)
                    except ClassNotFound:
                        lexer = get_lexer_by_name("text")
            print("using lexer `%s'" % type(lexer))
            formatter = HtmlFormatter(**formatter_params)
            render_result_stream = StringIO()
            pygments.highlight(code, lexer, formatter, render_result_stream)
            html = render_result_stream.getvalue()
            s1, se, s2 = html.partition("</style>")
            style = s1[s1.index("<style") + len("<style type='text/css'>"):]
            body = s2[s2.index('</h2>') + len('</h2>'):]

            r = []
            r.append('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" '
                     '"http://www.w3.org/TR/html4/strict.dtd">\n')
            r.append('<html>\n<head>\n')
            r.append('<meta http-equiv="Content-Type" '
                     'content="text/html; charset=utf-8">\n')
            r.append("<style type='text/css'>\n")
            r.append("\nbody { margin: 1em; font-size: 13px; }\n")
            r.append(".highlight pre { border: 1px solid #ccc; }\n")
            r.append(".highlight pre { padding: 1em; }\n")
            r.append(".highlight pre { word-wrap: break-word; }\n");
            r.append(style)
            r.append("</style>\n")
            r.append('</head>')
            r.append('<body>')
            r.append(body)
            r.append('</body>')
            r.append('</html>')
            render_result = "".join(r)

            fs = os.fstat(f.fileno())
            contents = render_result.encode('utf-8')
            return fs, contents, len(contents)


class SimplePygmentizeHTTPRequestHandler(simplehttpserver_mod.SimpleHTTPRequestHandler):
    _formatter = Formatter()
    _root = os.path.normpath(os.path.abspath(os.curdir))

    def list_directory(self, path):
        """Helper to produce a directory listing (absent index.html).

        Return value is either a file object, or None (indicating an
        error).  In either case, the headers are sent, making the
        interface the same as for send_head().

        """
        enc_i = sys.stdout.encoding
        try:
            if hasattr("", "decode"):
                list = [os.path.join(path, p.decode(enc_i))
                        for p in os.listdir(path)]
            else:
                list = [os.path.join(path, p)
                        for p in os.listdir(path)]
        except OSError:
            self.send_error(404, "No permission to list directory")
            return None
        list.sort(key=lambda a: (not os.path.isdir(a), a.lower()))
        if os.path.normpath(path) != self._root:
            list.insert(0, "..")
        r = []
        displaypath = unquote(path)
        displaypath = escape(displaypath.replace(os.path.sep, "/"))
        title = 'Directory listing for %s' % displaypath
        r.append('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" '
                 '"http://www.w3.org/TR/html4/strict.dtd">')
        r.append('<html>\n<head>')
        r.append('<meta http-equiv="Content-Type" '
                 'content="text/html; charset=utf-8">')
        r.append('<title>%s</title>\n</head>' % title)
        r.append('<body>\n<h1>%s</h1>' % title)
        r.append('<hr>\n<ul>')
        for fullname in list:
            name = os.path.basename(fullname)
            displayname = name
            # Append / for directories or @ for symbolic links
            if os.path.isdir(fullname):
                displayname = name + "/"
                linkname = name + "/"
                linkname = quote(linkname.encode('utf-8'))
            else:
                opts = self._formatter.options
                linkname = "?".join([
                        quote(name.encode('utf-8')),
                        "&".join(
                            [k + "=" + opts[k] for k in opts if opts[k]])])
            if os.path.islink(fullname):
                displayname = name + "@"
                # Note: a link to a directory displays with @ and links with /
            r.append('<li><a href="%s">%s</a></li>'
                    % (linkname,
                       escape(displayname)))
        r.append('</ul>\n<hr>\n</body>\n</html>\n')
        encoded = '\n'.join(r).encode('utf-8')
        f = BytesIO()
        f.write(encoded)
        f.seek(0)
        self.send_response(200)
        self.send_header("Content-type", "text/html; charset=utf-8")
        self.send_header("Content-Length", str(len(encoded)))
        self.end_headers()
        return f

    def send_head(self):
        fmtparams = dict([
            kv.partition("=")[::2]
            for kv in urlparse(self.path).query.split('&')])

        path = self.translate_path(self.path)
        if os.path.isdir(path):
            if not self.path.endswith('/'):
                # redirect browser - doing basically what apache does
                self.send_response(301)
                self.send_header("Location", self.path + "/")
                self.end_headers()
                return None
            return self.list_directory(path)
        ctype = "text/html"
        try:
            fs, contents, content_length = self._formatter(path, **fmtparams)
            self.send_response(200)
            self.send_header("Content-type", ctype)
            self.send_header("Content-Length", len(contents))
            self.send_header("Last-Modified", self.date_time_string(fs.st_mtime))
            self.end_headers()
            self.wfile.write(contents)
        except IOError:
            self.send_error(404, "File not found")


def test(HandlerClass=basehttpserver_mod.BaseHTTPRequestHandler,
         ServerClass=basehttpserver_mod.HTTPServer,
         protocol="HTTP/1.0", port=8000, bind=""):
    """Test the HTTP request handler class.

    This runs an HTTP server on port 8000 (or the first command line
    argument).

    """
    server_address = (bind, port)

    HandlerClass.protocol_version = protocol
    httpd = ServerClass(server_address, HandlerClass)

    sa = httpd.socket.getsockname()
    print("Serving HTTP on", sa[0], "port", sa[1], "...")
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        print("\nKeyboard interrupt received, exiting.")
        httpd.server_close()
        sys.exit(0)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--bind', '-b', default='', metavar='ADDRESS',
        help='Specify alternate bind address '
        '[default: all interfaces]')
    parser.add_argument(
        '--root', '-r', default='',
        help='Specify root directory '
        '[default: current directory]')
    parser.add_argument(
        '--style', '-s', default='',
        help='Specify style using if style is not specified in query'
        '[default: default]')
    parser.add_argument(
        '--linenos', '-l', choices=['table', 'inline'])
    parser.add_argument(
        'port', action='store',
        default=8000, type=int,
        nargs='?',
        help='Specify alternate port [default: 8000]')
    args = parser.parse_args()
    if args.root:
        os.chdir(os.path.abspath(args.root))
    SimplePygmentizeHTTPRequestHandler._root = os.path.normpath(
        os.path.abspath(os.curdir))
    formatter_args = {}
    if args.style:
        formatter_args["style"] = args.style
    if args.linenos:
        formatter_args["linenos"] = args.linenos
    SimplePygmentizeHTTPRequestHandler._formatter = Formatter(**formatter_args)
    test(HandlerClass=SimplePygmentizeHTTPRequestHandler,
         port=args.port,
         bind=args.bind)
