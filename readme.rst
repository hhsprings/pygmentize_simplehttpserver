Pygmentize Simple HTTPServer
============================
* Require: `Pygments <http://pygments.org/>`_

* Install:

.. code-block:: console

  you@yourpc: ~$ tar zxvf pygmentize_simplehttpserver.X.Y.tar.gz
  you@yourpc: ~$ cd pygmentize_simplehttpserver.X.Y
  you@yourpc: pygmentize_simplehttpserver.X.Y$ python setup.py build
  you@yourpc: pygmentize_simplehttpserver.X.Y$ su  # if you are using on Unix
    ******
  you@yourpc: pygmentize_simplehttpserver.X.Y# python setup.py install

* Usage:

.. code-block:: console

  you@yourpc: ~$ python -m pygmentize_simplehttpserver.server --root=../.. --style=vim


Use it local on your PC only, and don't serve to world-wide as real server.
